import java.util.ArrayList;

public class DirectoryIndexed {
	private String name;
	private ArrayList<FileIndexed> files;
	private ArrayList<DirectoryIndexed> directories;
	
	public DirectoryIndexed(String name) {
		this.name = name;
		files = new ArrayList<FileIndexed>();
		directories = new ArrayList<DirectoryIndexed>();
	}
	public boolean addFile(String fileName, int[] blocks){
		for(FileIndexed f:files)
			if(f.getName().equals(fileName))
				return false;
		
		FileIndexed newFile = new FileIndexed(fileName, blocks);
		files.add(newFile);
		return true;
	}

	public FileIndexed deleteFile(String fileName){
		for(int i=0; i<files.size(); i++){
			if(files.get(i).getName().equals(fileName)){
				FileIndexed dummy = new FileIndexed(fileName, files.get(i).getBlocks());
				files.remove(i);
				return dummy;
			}
		}
		return null;
	}

	public boolean deleteDir(String dirName) {
		for(DirectoryIndexed dir:directories)
			if(dir.getName().equals(dirName)) {
				for(int ind=0; ind< dir.files.size(); ) {
					FileIndexed file = dir.files.get(ind);
					FileIndexed dummy = dir.deleteFile(file.getName());
					if(dummy != null){
						int[] blocks = dummy.getBlocks();
						for(int i:blocks)
							Demo.disk2[i] = false;
						Demo.allocated2 -= dummy.getBlocks().length;
					}
				}
				for(int i=0; i<dir.directories.size(); ){
					dir.deleteDir(dir.directories.get(i).getName());
				}
				
				return directories.remove(dir);
			}
		
		return false;
	}

	public boolean addDir(String dirName) {
		for(DirectoryIndexed dir:directories)
			if(dir.getName().equals(dirName))
				return false;
		
		DirectoryIndexed newDir = new DirectoryIndexed(dirName);
		directories.add(newDir);
		return true;
	}
	
	DirectoryIndexed find(String dest){
		for(DirectoryIndexed dir:directories){
			if(dir.name.equals(dest))
				return dir;
		}
		return null;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<FileIndexed> getFiles() {
		return files;
	}
	public void setFiles(ArrayList<FileIndexed> files) {
		this.files = files;
	}
	public ArrayList<DirectoryIndexed> getDirectories() {
		return directories;
	}
	public void setDirectories(ArrayList<DirectoryIndexed> directories) {
		this.directories = directories;
	}
}
