import java.util.ArrayList;

public class DirectoryContiguous {
	private String name;
	private ArrayList<FileContiguous> files;
	private ArrayList<DirectoryContiguous> directories;
	private int length;

	public DirectoryContiguous() {
		files = new  ArrayList<FileContiguous>();
		directories = new ArrayList<DirectoryContiguous>();
	}
	public DirectoryContiguous(String dirName) {
		name = dirName;
		files = new  ArrayList<FileContiguous>();
		directories = new ArrayList<DirectoryContiguous>();
	}
	
	DirectoryContiguous find(String dest){
		for(DirectoryContiguous dir:directories){
			if(dir.name.equals(dest))
				return dir;
		}
		return null;
	}
	
	public boolean addFile(String fileName, int startingIndex, int size){
		for(FileContiguous f:files)
			if(f.getName().equals(fileName))
				return false;
		
		FileContiguous newFile = new FileContiguous(fileName, startingIndex, size);
		files.add(newFile);
		return true;
	}
	
	public boolean addDir(String dirName) {
		for(DirectoryContiguous dir:directories)
			if(dir.getName().equals(dirName))
				return false;
		
		DirectoryContiguous newDir = new DirectoryContiguous(dirName);
		directories.add(newDir);
		return true;
	}
	
	public boolean deleteDir(String dirName) {
		for(DirectoryContiguous dir:directories)
			if(dir.getName().equals(dirName)) {
				for(int ind=0; ind< dir.files.size();) {
					FileContiguous file = dir.files.get(ind);
					FileContiguous dummy = dir.deleteFile(file.getName());
					if(dummy != null){
						for(int i=dummy.getStartingIndex(); i<dummy.getStartingIndex()+dummy.getLength(); i++)
							Demo.disk[i] = false; // freed
						Demo.allocated1 -= dummy.getLength();
					}
				}
				for(int i=0; i<dir.directories.size(); ){
					dir.deleteDir(dir.directories.get(i).getName());
				}
				
				return directories.remove(dir);
			}
		
		return false;
	}

	public FileContiguous deleteFile(String fileName){
		for(int i=0; i<files.size(); i++){
			if(files.get(i).getName().equals(fileName)){
				FileContiguous dummy = new FileContiguous(fileName, files.get(i).getStartingIndex(), files.get(i).getLength());
				files.remove(i);
				return dummy;
			}
		}
		return null;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<FileContiguous> getFiles() {
		return files;
	}
	public void setFiles(ArrayList<FileContiguous> files) {
		this.files = files;
	}
	public ArrayList<DirectoryContiguous> getDirectories() {
		return directories;
	}
	public void setDirectories(ArrayList<DirectoryContiguous> directories) {
		this.directories = directories;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
}