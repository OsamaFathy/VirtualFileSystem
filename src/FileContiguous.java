import java.util.Arrays;

public class FileContiguous {
	private String name;
	private int startingIndex;
	private int length;
	
	FileContiguous(){
		
	}
	
	FileContiguous(String name, int startingIndex, int length){
		this.name = name;
		this.startingIndex = startingIndex;
		this.length = length;
	}
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getStartingIndex() {
		return startingIndex;
	}

	public void setStartingIndex(int startingIndex) {
		this.startingIndex = startingIndex;
	}
}
