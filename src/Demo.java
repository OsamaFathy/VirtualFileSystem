import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.Buffer;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Demo {
	static boolean[] disk;
	static boolean[] disk2;
	static DirectoryContiguous root1;
	static DirectoryIndexed root2;
	static int allocated1 = 0;
	static int allocated2 = 0;
	static int diskSize1 = 1000000;
	static int diskSize2 = 1000000;
	static User currentUser;
	static ArrayList<User> users;
	static ArrayList<Capability> capabilities;
	static String[] parsePath(String path) {
		path = path.trim();
		String[] list = path.split("\\\\");
		return list;
	}

	// =============== Destination Directory ===============
	public static DirectoryContiguous getDestinationDirectoryContiguous(String[] parsedString) {
		if (!parsedString[0].equals("root")) {
			return null;
		}
		DirectoryContiguous cur = root1;
		int ind = 1;
		while (cur != null && ind < parsedString.length - 1) {
			cur = cur.find(parsedString[ind++]);
		}
		return cur;
	}

	public static DirectoryIndexed getDestinationDirectoryIndexed(String[] parsedString) {
		if (!parsedString[0].equals("root")) {
			return null;
		}
		DirectoryIndexed cur = root2;
		int ind = 1;
		while (cur != null && ind < parsedString.length - 1) {
			cur = cur.find(parsedString[ind++]);
		}
		return cur;
	}

	// =============== Add File ===============
	public static int addFileContiguous(String[] parsedString, int size) {
		DirectoryContiguous cur = getDestinationDirectoryContiguous(parsedString);
		if (cur == null)
			return 0;
		
		int startingIndex = getAddress(size);
		if (startingIndex == -1)
			return 1;

		if (!cur.addFile(parsedString[parsedString.length - 1], startingIndex, size))
			return 2;

		for (int i = startingIndex; i < startingIndex + size; i++) {
			disk[i] = true;
		}
		allocated1 += size;
		return 3;
	}

	public static int addFileIndexed(String[] parsedString, int size) {
		DirectoryIndexed cur = getDestinationDirectoryIndexed(parsedString);
		if (cur == null)
			return 0;
		if (diskSize2 - allocated2 < size)
			return 1;

		int ind = 0;
		int[] blocks = new int[size];
		for (int i = 0; i < diskSize2 && ind < size; i++) {
			if (!disk2[i]) {
				blocks[ind++] = i;
			}
		}

		if (!cur.addFile(parsedString[parsedString.length - 1], blocks))
			return 2;

		for (int num : blocks) {
			disk2[num] = true;
		}
		allocated2 += size;
		return 3;
	}

	// =============== Add Directory ===============
	public static int addDirectoryContiguous(String[] parsedString) {
		DirectoryContiguous cur = getDestinationDirectoryContiguous(parsedString);
		if (cur == null)
			return 0;
		if (!cur.addDir(parsedString[parsedString.length - 1]))
			return 1;
		return 2;
	}

	public static int addDirectoryIndexed(String[] parsedString) {
		DirectoryIndexed cur = getDestinationDirectoryIndexed(parsedString);
		if (cur == null)
			return 0;
		if (!cur.addDir(parsedString[parsedString.length - 1]))
			return 1;
		return 2;
	}

	// =============== Delete Directory ===============
	public static int deleteDirectoryContiguous(String[] parsedString) {
		DirectoryContiguous cur = getDestinationDirectoryContiguous(parsedString);
		if (cur == null)
			return 0;
		if (!cur.deleteDir(parsedString[parsedString.length - 1]))
			return 1;

		return 2;
	}

	public static int deleteDirectoryIndexed(String[] parsedString) {
		DirectoryIndexed cur = getDestinationDirectoryIndexed(parsedString);
		if (cur == null)
			return 0;
		if (!cur.deleteDir(parsedString[parsedString.length - 1]))
			return 1;

		return 2;
	}

	// =============== Delete File ===============
	public static boolean deleteFileContiguous(String[] parsedString) {
		DirectoryContiguous cur = getDestinationDirectoryContiguous(parsedString);
		if (cur == null)
			return false;
		FileContiguous dummy = cur.deleteFile(parsedString[parsedString.length - 1]);
		if (dummy == null)
			return false;

		for (int i = dummy.getStartingIndex(); i < dummy.getStartingIndex() + dummy.getLength(); i++)
			disk[i] = false; // freed
		allocated1 -= dummy.getLength();
		return true;
	}

	public static boolean deleteFileIndexed(String[] parsedString) {
		DirectoryIndexed cur = getDestinationDirectoryIndexed(parsedString);
		if (cur == null)
			return false;
		FileIndexed dummy = cur.deleteFile(parsedString[parsedString.length - 1]);
		if (dummy == null)
			return false;

		int[] blocks = dummy.getBlocks();
		for (int i : blocks)
			disk2[i] = false;
		allocated2 -= dummy.getBlocks().length;
		return true;
	}

	// =============== Get Address ===============
	public static int getAddress(int size) {
		int mxFree = 0;
		int mxInd = -1;

		int curInd = 0;
		int num = 0;
		for (int i = 0; i < 1000000; i++) {
			if (!disk[i] && (i == 0 || disk[i - 1])) {
				curInd = i;
				num = 1;
			} else if (!disk[i])
				num++;
			else {
				if (num > mxFree) {
					mxFree = num;
					mxInd = curInd;
				}
				num = 0;
			}

		}
		if (num > mxFree) {
			mxFree = num;
			mxInd = curInd;
		}
		num = 0;

		return mxFree >= size ? mxInd : -1;
	}

	// =============== Display Disk Structure ===============
	public static void displayDiskStructureContiguous(DirectoryContiguous dir, int spaces) {
		for (int i = 0; i < spaces; i++)
			System.out.print("  ");
		System.out.println("<" + dir.getName() + ">");

		ArrayList<DirectoryContiguous> dirs = dir.getDirectories();
		ArrayList<FileContiguous> files = dir.getFiles();
		for (FileContiguous f : files) {
			for (int i = 0; i < spaces + 1; i++)
				System.out.print("  ");
			System.out.println(f.getName());
		}
		for (DirectoryContiguous d : dirs) {
			displayDiskStructureContiguous(d, spaces + 1);
		}
	}

	public static void displayDiskStructureIndexed(DirectoryIndexed dir, int spaces) {
		for (int i = 0; i < spaces; i++)
			System.out.print("  ");
		System.out.println("<" + dir.getName() + ">");

		ArrayList<DirectoryIndexed> dirs = dir.getDirectories();
		ArrayList<FileIndexed> files = dir.getFiles();
		for (FileIndexed f : files) {
			for (int i = 0; i < spaces + 1; i++)
				System.out.print("  ");
			System.out.println(f.getName());
		}
		for (DirectoryIndexed d : dirs) {
			displayDiskStructureIndexed(d, spaces + 1);
		}
	}

	// =============== Display Disk Status ===============
	public static void displayDiskStatusContiguous() {
		System.out.println("Empty Space:\t\t" + (diskSize1 - allocated1));
		System.out.println("Allocated Space:\t" + allocated1);

		int num = 0;
		int from = -1;

		System.out.println("Empty Blocks:");
		for (int i = 0; i <= diskSize1; i++) {
			if (i == diskSize1 || disk[i]) {
				if (from != -1 || (i == diskSize1 && from != -1)) {
					System.out.println("From " + from + " To " + (from + num - 1));
					num = 0;
					from = -1;
				}
			} else {
				if (from == -1)
					from = i;
				num++;
			}
		}
		System.out.println("");
		System.out.println("Allocated Blocks:");
		for (int i = 0; i <= diskSize1; i++) {
			if (i == diskSize1 || !disk[i]) {
				if (from != -1 || (i == diskSize1 && from != -1)) {
					System.out.println("From " + from + " To " + (from + num - 1));
					num = 0;
					from = -1;
				}
			} else {
				if (from == -1)
					from = i;
				num++;
			}
		}
	}

	public static void displayDiskStatusIndexed() {
		System.out.println("Empty Space:\t\t" + (diskSize2 - allocated2));
		System.out.println("Allocated Space:\t" + allocated2);

		int num = 0;
		int from = -1;

		System.out.println("Empty Blocks:");
		for (int i = 0; i <= diskSize2; i++) {
			if (i == diskSize2 || disk2[i]) {
				if (from != -1 || (i == diskSize2 && from != -1)) {
					System.out.println("From " + from + " To " + (from + num - 1));
					num = 0;
					from = -1;
				}
			} else {
				if (from == -1)
					from = i;
				num++;
			}
		}
		System.out.println("");
		System.out.println("Allocated Blocks:");
		for (int i = 0; i <= diskSize2; i++) {
			if (i == diskSize2 || !disk2[i]) {
				if (from != -1 || (i == diskSize2 && from != -1)) {
					System.out.println("From " + from + " To " + (from + num - 1));
					num = 0;
					from = -1;
				}
			} else {
				if (from == -1)
					from = i;
				num++;
			}
		}
	}
	// =============== Export ===============

	public static void getDirs(ArrayList<String> folders, DirectoryContiguous dir, String path) {
		if (!path.equals("root"))
			folders.add(path);
		ArrayList<DirectoryContiguous> dirs = dir.getDirectories();
		for (DirectoryContiguous d : dirs) {
			String curPath = path + "\\" + d.getName();
			getDirs(folders, d, curPath);
		}
	}

	public static void getFiles(ArrayList<String> files, DirectoryContiguous dir, String path) {
		ArrayList<DirectoryContiguous> dirs = dir.getDirectories();
		ArrayList<FileContiguous> fs = dir.getFiles();
		for (FileContiguous f : fs)
			files.add(path + "\\" + f.getName() + " " + f.getStartingIndex() + " " + f.getLength());

		for (DirectoryContiguous d : dirs) {
			String curPath = path + "\\" + d.getName();
			getFiles(files, d, curPath);
		}
	}

	public static void getDirs2(ArrayList<String> folders, DirectoryIndexed dir, String path) {
		if (!path.equals("root"))
			folders.add(path);
		ArrayList<DirectoryIndexed> dirs = dir.getDirectories();
		for (DirectoryIndexed d : dirs) {
			String curPath = path + "\\" + d.getName();
			getDirs2(folders, d, curPath);
		}
	}

	public static void getFiles2(ArrayList<String> files, DirectoryIndexed dir, String path) {
		ArrayList<DirectoryIndexed> dirs = dir.getDirectories();
		ArrayList<FileIndexed> fs = dir.getFiles();
		for (FileIndexed f : fs) {
			String str = path + "\\" + f.getName() + " ";
			int[] blocks = f.getBlocks();
			str += blocks.length + " ";
			for (int i = 0; i < blocks.length; i++)
				str += blocks[i] + " ";

			files.add(str);
		}

		for (DirectoryIndexed d : dirs) {
			String curPath = path + "\\" + d.getName();
			getFiles2(files, d, curPath);
		}
	}

	public static void exportContiguous(String path) {
		BufferedWriter bf = null;
		try {
			path = path.trim();

			bf = new BufferedWriter(new FileWriter(path));
			bf.write("-Folders\n");

			ArrayList<String> dirs = new ArrayList<String>();
			ArrayList<String> files = new ArrayList<String>();
			getDirs(dirs, root1, "root");
			getFiles(files, root1, "root");

			for (String curDir : dirs) {
				bf.write(curDir + "\n");
			}
			bf.write("-Files\n");
			for (String curF : files) {
				bf.write(curF + "\n");
			}

			bf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void exportIndexed(String path) {
		BufferedWriter bf = null;
		try {
			path = path.trim();

			bf = new BufferedWriter(new FileWriter(path, true));
			bf.write("-------------------\n");

			bf.write("-Folders\n");

			ArrayList<String> dirs = new ArrayList<String>();
			ArrayList<String> files = new ArrayList<String>();
			getDirs2(dirs, root2, "root");
			getFiles2(files, root2, "root");
			// System.out.println("Here1");
			for (String curDir : dirs) {
				bf.write(curDir + "\n");
			}
			// System.out.println("Here1");
			bf.write("-Files\n");
			for (String curF : files) {
				bf.write(curF + "\n");
				// System.out.println(curF);
			}
			// System.out.println("Here1");

			bf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void exportUsers(String path) {
		BufferedWriter bf = null;
		try {
			path = path.trim();
			while (path.length() > 0 && path.charAt(path.length() - 1) != '\\') {
				path = path.substring(0, path.length() - 1);
			}
			path += "user.txt";
			bf = new BufferedWriter(new FileWriter(path, true));
			for (User u : users) {
				bf.write(u.getUsername() + " " + u.getPassword() + "\n");
			}
			bf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void exportCapabilities(String path) {
		BufferedWriter bf = null;
		try {
			path = path.trim();
			while (path.length() > 0 && path.charAt(path.length() - 1) != '\\') {
				path = path.substring(0, path.length() - 1);
			}
			path += "capability.txt";
			bf = new BufferedWriter(new FileWriter(path, true));
			for (Capability c : capabilities) {
				bf.write(c.getPath() + " ");
				for (int i = 0; i < c.getUsers().size(); i++)
					bf.write(c.getUsers().get(i) + " " + c.getTypeOfCap().get(i) + " ");
			}
			bf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// =============== Import ===============
	public static BufferedReader ImportContiguous(String path) {
		BufferedReader bf = null;
		path = path.trim();
		try {
			bf = new BufferedReader(new FileReader(path));

			bf.readLine();
			root1 = new DirectoryContiguous("root");

			while (true) {
				String rPath = bf.readLine();
				if (rPath.equals("-Files"))
					break;
				addDirectoryContiguous(parsePath(rPath));
			}
			String curLine;
			while (!(curLine = bf.readLine()).equals("-------------------")) {
				Scanner sc = new Scanner(curLine);
				String rPath = sc.next();
				int startingIndex = sc.nextInt();
				int length = sc.nextInt();

				for (int i = startingIndex; i < startingIndex + length; i++)
					disk[i] = true;

				String[] parsedPath = parsePath(rPath);
				DirectoryContiguous parent = getDestinationDirectoryContiguous(parsedPath);
				parent.addFile(parsedPath[parsedPath.length - 1], startingIndex, length);
				allocated1 += length;
			}
			return bf;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void ImportIndexed(BufferedReader bf) {
		try {

			bf.readLine();
			root2 = new DirectoryIndexed("root");

			while (true) {
				String rPath = bf.readLine();
				if (rPath.equals("-Files"))
					break;
				addDirectoryIndexed(parsePath(rPath));
			}
			String curLine;
			while ((curLine = bf.readLine()) != null) {
				Scanner sc = new Scanner(curLine);
				String rPath = sc.next();

				int length = sc.nextInt();
				int[] blocks = new int[length];
				for (int i = 0; i < length; i++) {
					int bl = sc.nextInt();
					disk2[bl] = true;
					blocks[i] = bl;
				}
				String[] parsedPath = parsePath(rPath);
				DirectoryIndexed parent = getDestinationDirectoryIndexed(parsedPath);

				parent.addFile(parsedPath[parsedPath.length - 1], blocks);
				allocated2 += length;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void ImportUsers(String path) {
		users = new ArrayList<User>();
		try {
			path = path.trim();
			while (path.length() > 0 && path.charAt(path.length() - 1) != '\\') {
				path = path.substring(0, path.length() - 1);
			}
			path += "user.txt";
			Scanner sf = new Scanner(new File(path));
			while (sf.hasNext()) {
				String user = sf.next();
				String pass = sf.next();
				users.add(new User(user, pass));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void ImportCapabilities(String path) {
		capabilities = new ArrayList<Capability>();
		try {
			path = path.trim();
			while (path.length() > 0 && path.charAt(path.length() - 1) != '\\') {
				path = path.substring(0, path.length() - 1);
			}
			path += "capability.txt";
			Scanner sf = new Scanner(new File(path));
			while (sf.hasNext()) {
				String curLine = sf.nextLine();
				Scanner sf2 = new Scanner(curLine);
				String p = sf2.next();
				Capability cp = new Capability();
				cp.setPath(p);
				while (sf2.hasNext()) {
					String user = sf2.next();
					int type = sf2.nextInt();
					cp.addCapability(new User(user, ""), type);
				}
				capabilities.add(cp);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		disk = new boolean[1000000];
		disk2 = new boolean[1000000];
		root1 = new DirectoryContiguous("root");
		root2 = new DirectoryIndexed("root");
		currentUser = new User("admin", "admin");
		users = new ArrayList<User>();
		users.add(currentUser);
		capabilities = new ArrayList<Capability>();
		Capability capab = new Capability("root"); 
		capab.addCapability(currentUser, 3);
		capabilities.add(capab);

		Scanner sc = new Scanner(System.in);
		while (true) {
			String command = sc.next();
			String path = sc.nextLine();

			//----------Create File--------------
			if (command.equals("CreateFile")) {
				int size = 0;
				int num = 1;
				int end = path.length();
				for (end = path.length() - 1; end >= 0; end--) {
					if (path.charAt(end) == ' ')
						break;
					size += (path.charAt(end) - '0') * num;
					num *= 10;
				}

				path = path.substring(0, end);
				String[] parsedString = parsePath(path);
				
				boolean permission = false,found = false;
				String dest = "";
				int idx = 0;
				for(;idx < parsedString.length-1;++idx) {
					dest += parsedString[idx];
					if(idx < parsedString.length-2) dest += "\\";
				}
				
				for(Capability cap : capabilities) 
					if(cap.getPath().equals(dest)) {
						found = true;
						ArrayList<User> Users = cap.getUsers();
						ArrayList<Integer> Types = cap.getTypeOfCap();
						for(int i = 0; i < Users.size(); ++i) 
							if(Users.get(i).getUsername().equals(currentUser.getUsername())) 
								if(Types.get(i) == 1 || Types.get(i) == 3) {
									permission = true;
									break;
								}
						break;
					}
				
				if(!found) {
					System.out.println("No such directory!");
					continue;
				}
				
				permission |= currentUser.getUsername() == "admin";
				if(!permission) {
					System.out.println("Sorry you are not allowed to create files in this directory.");
					continue;
				}
				
				int state = addFileContiguous(parsedString, size);
				System.out.print("Contiguous: ");
				if (state == 0)
					System.out.println("Directory not found");
				else if (state == 1)
					System.out.println("No Available size");
				else if (state == 2)
					System.out.println("Already exists");
				else
					System.out.println("Done");

				System.out.print("Indexed: ");
				state = addFileIndexed(parsedString, size);
				if (state == 0)
					System.out.println("Directory not found");
				else if (state == 1)
					System.out.println("No Available size");
				else if (state == 2)
					System.out.println("Already exists");
				else
					System.out.println("Done");

				
				//-----------Delete File-----------
			} else if (command.equals("DeleteFile")) {
				String[] parsedString = parsePath(path);
				
				boolean permission = false,found = false;
				String dest = "";
				int idx = 0;
				for(;idx < parsedString.length-1;++idx) {
					dest += parsedString[idx];
					if(idx < parsedString.length-2) dest += "\\";
				}
				
				for(Capability cap : capabilities) 
					if(cap.getPath().equals(dest)) {
						found = true;
						ArrayList<User> Users = cap.getUsers();
						ArrayList<Integer> Types = cap.getTypeOfCap();
						for(int i = 0; i < Users.size(); ++i) 
							if(Users.get(i).getUsername().equals(currentUser.getUsername())) 
								if(Types.get(i) == 2 || Types.get(i) == 3) {
									permission = true;
									break;
								}
						break;
					}
				
				if(!found) {
					System.out.println("No such directory!");
					continue;
				}
				
				permission |= currentUser.getUsername() == "admin";
				if(!permission) {
					System.out.println("Sorry you are not allowed to delete files in this directory.");
					continue;
				}
				
				boolean state = deleteFileContiguous(parsedString);								
				System.out.print("Contiguous: ");
				if (!state)
					System.out.println("File not found");
				else
					System.out.println("Done");

				System.out.print("Indexed: ");
				state = deleteFileIndexed(parsedString);
				if (!state)
					System.out.println("File not found");
				else
					System.out.println("Done");

			} else if (command.equals("DisplayDiskStructure")) {
				System.out.println("Contiguous:");
				displayDiskStructureContiguous(root1, 0);
				System.out.println("Indexed:");
				displayDiskStructureIndexed(root2, 0);
				
				
				//--------Create Folder--------
			} else if (command.equals("CreateFolder")) { 
				String[] parsedString = parsePath(path);
				
				boolean permission = false,found = false;
				
				String dest = "";
				int idx = 0;
				for(;idx < parsedString.length-1;++idx) {
					dest += parsedString[idx];
					if(idx < parsedString.length-2) dest += "\\";
				}
				
				for(Capability cap : capabilities) 
					if(cap.getPath().equals(dest)) {
						found = true;
						ArrayList<User> Users = cap.getUsers();
						ArrayList<Integer> Types = cap.getTypeOfCap();
						for(int i = 0; i < Users.size(); ++i) 
							if(Users.get(i).getUsername().equals(currentUser.getUsername())) 
								if(Types.get(i) == 1 || Types.get(i) == 3) {
									permission = true;
									break;
								}
						break;
					}
				
				if(!found) {
					System.out.println("No such directory!");
					continue;
				}
				
				permission |= currentUser.getUsername() == "admin";
				if(!permission) {
					System.out.println("Sorry you are not allowed to create folders in this directory.");
					continue;
				}
				
				int state = addDirectoryContiguous(parsedString);
				System.out.print("Contiguous: ");
				if (state == 0)
					System.out.println("Directory not found");
				else if (state == 1)
					System.out.println("Directory is already exists");
				else 
					System.out.println("Done");

				int state2 = addDirectoryIndexed(parsedString);
				System.out.print("Indexed: ");
				if (state2 == 0)
					System.out.println("Directory not found");
				else if (state2 == 1)
					System.out.println("Directory is already exists");
				else
					System.out.println("Done");
				
				if(state > 1 && state2 > 1) {
					dest += "\\" + parsedString[idx];
					Capability newCap = new Capability(dest); 
					newCap.addCapability(currentUser, 3);
					capabilities.add(newCap);
				}

			} else if (command.equals("DisplayDiskStatus")) {
				System.out.println("Contiguous:");
				displayDiskStatusContiguous();
				System.out.println("\n\nIndexed:");
				displayDiskStatusIndexed();
				
				//----------Delete Folder------------
			} else if (command.equals("DeleteFolder")) {
				String[] parsedString = parsePath(path);
				
				boolean permission = false,found = false;
				String dest = "";
				int idx = 0;
				for(;idx < parsedString.length-1;++idx) {
					dest += parsedString[idx];
					if(idx < parsedString.length-2) dest += "\\";
				}
				
				Capability toDelete = null;
				for(Capability cap : capabilities) 
					if(cap.getPath().equals(dest)) {
						toDelete = cap;
						found = true;
						ArrayList<User> Users = cap.getUsers();
						ArrayList<Integer> Types = cap.getTypeOfCap();
						for(int i = 0; i < Users.size(); ++i) 
							if(Users.get(i).getUsername().equals(currentUser.getUsername())) 
								if(Types.get(i) == 2 || Types.get(i) == 3) {
									permission = true;
									break;
								}
						break;
					}
				
				if(!found) {
					System.out.println("No such directory!");
					continue;
				}
				
				permission |= currentUser.getUsername() == "admin";
				if(!permission) {
					System.out.println("Sorry you are not allowed to delete folders in this directory.");
					continue;
				}
				
				int state = deleteDirectoryContiguous(parsedString);
				System.out.print("Contiguous: ");
				if (state == 0)
					System.out.println("Directory not found");
				else if (state == 1)
					System.out.println("Directory does not exist");
				else
					System.out.println("Done");

				int state2 = deleteDirectoryIndexed(parsedString);
				System.out.print("Indexed: ");
				if (state == 0)
					System.out.println("Directory not found");
				else if (state == 1)
					System.out.println("Directory does not exist");
				else
					System.out.println("Done");
				
				if(state > 1 && state2 > 1 && toDelete != null) {
					capabilities.remove(toDelete);
				}
				
				//--------Export--------
			} else if (command.equals("Export")) {
				exportContiguous(path);
				exportIndexed(path);
				exportUsers(path);
				exportCapabilities(path);
				
				//--------Import--------
			} else if (command.equals("Import")) {
				Arrays.fill(disk, false);
				Arrays.fill(disk2, false);
				allocated1 = 0;
				allocated2 = 0;
				BufferedReader bf = ImportContiguous(path);
				ImportIndexed(bf);

			}else if(command.equals("TellUser")) {
				System.out.println(currentUser.getUsername());
			}else if (command.equals("Exit")) {

				ImportUsers(path);
				ImportCapabilities(path);
				
				//-------Create User----------
			} else if (command.equals("cUser")) {
				path = path.trim();
				if (currentUser.getUsername().equals("admin")) {
					int i = 0;
					for (i = 0; i < path.length(); i++) {
						if (path.charAt(i) == ' ')
							break;
					}
					if (i != path.length()) {
						String user = path.substring(0, i);
						String pass = path.substring(i + 1);
						users.add(new User(user, pass));
					}
				} else {
					System.out.println("Only admin can create users.");
				}
			} else if (command.equals("Grant")) {
				path = path.trim();
				if (currentUser.getUsername().equals("admin")) {
					String user = "";
					String cap = "";
					int i = 0;
					for (i = 0; i < path.length(); i++) {
						if (path.charAt(i) == ' ')
							break;
					}

					if (i != path.length()) {
						user = path.substring(0, i);
						path = path.substring(i + 1);
					}
					for (i = 0; i < path.length(); i++) {
						if (path.charAt(i) == ' ')
							break;
					}

					if (i != path.length()) {
						cap = path.substring(i + 1);
						path = path.substring(0, i);
					}
					
					boolean found = false;
					User tmp = null;
					for (User u : users) {
						if (u.getUsername().equals(user)) {
							tmp = u;
							break;
						}
					}
					if (tmp != null)
						for (Capability c : capabilities) {
							if (c.getPath().equals(path)) {
								c.addCapability(tmp, Integer.parseInt(cap));
								found = true;
								break;
							}
						}
					if (found) {
						System.out.println("The user has been granted the permission");
					} else {
						System.out.println("Path or user not found");

					}
				} else {
					System.out.println("Only admin can grant users permissions.");
				}
			} else if (command.equals("Login")) {
				path = path.trim();
				int i = 0;
				String user = "", pass = "";
				for (i = 0; i < path.length(); i++) {
					if (path.charAt(i) == ' ')
						break;
				}

				if (i != path.length()) {
					user = path.substring(0, i);
					pass = path.substring(i + 1);
				}

				for (User u : users) {
					if (u.getUsername().equals(user) && u.getPassword().equals(pass)) {
						currentUser = u;
						break;
					}
				}
				if (currentUser.getUsername().equals(user)) {
					System.out.println("Hello, " + user + "!");
				} else {
					System.out.println("Wrong username or password!");
				}
			} else if (command.equals("Exit")) {
				ArrayList<String> arr = new ArrayList<String>();
				break;
			}
		}
		sc.close();
	}
}
