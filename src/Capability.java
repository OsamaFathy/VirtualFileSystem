import java.util.ArrayList;

class Capability {
	private String path;
	private ArrayList<User> users;
	private ArrayList<Integer> typeOfCap;

	Capability() {
		users = new ArrayList<User>();
		typeOfCap = new ArrayList<Integer>();
	}
	
	Capability(String p) {
		path = p;
		users = new ArrayList<User>();
		typeOfCap = new ArrayList<Integer>();
	}

	Capability(String p, ArrayList<User> u, ArrayList<Integer> t) {
		users = new ArrayList<User>();
		typeOfCap = new ArrayList<Integer>();
		path = p;
		for (User user : u)
			users.add(new User(user.getUsername(), user.getPassword()));
		for (int cap : t)
			typeOfCap.add(cap);
	}
	void addCapability(User user, int type){
		users.add(user);
		typeOfCap.add(type);
	}
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

	public ArrayList<Integer> getTypeOfCap() {
		return typeOfCap;
	}

	public void setTypeOfCap(ArrayList<Integer> typeOfCap) {
		this.typeOfCap = typeOfCap;
	}
}