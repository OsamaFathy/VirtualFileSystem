import java.util.Arrays;

public class FileIndexed {
	private String name;
	private int[] blocks;
	FileIndexed(String name, int[] blocks){
		this.name = name;
		this.blocks = Arrays.copyOf(blocks, blocks.length);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int[] getBlocks() {
		return blocks;
	}
	public void setBlocks(int[] blocks) {
		this.blocks = Arrays.copyOf(blocks, blocks.length);
	}
	
}
